const request = require('supertest')
const app = require('../../src/app')

describe('StatusController', () => {
  it('Should be able to return a response for status', async () => {
    const response = await request(app)
      .get('/api/status')

    expect(response.status).toBe(200)
    expect(response.text).toEqual('ok')
  })
})
