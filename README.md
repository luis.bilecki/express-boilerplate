# Express Boilerplate

Boilerplate for building RESTful APIs using Node.js and Express.

---

## Features

TO-DO

## Scripts

TO-DO

## Getting started

### Prerequisites

This project is Docker based:

```
Docker >= 19
Docker-compose >= 1.21
```

Node.js and Yarn is required to run locally the Husky hooks.

```
Node.js >= 10 (see .nvmrc)
Yarn >= 1.22
```

## Usage

1. Clone this repository:

```
git clone https://gitlab.com/luis.bilecki/express-boilerplate.git
```

2. Build and create containers for services:

```
docker-compose up --build
```

3. To stop the running containers use:

```
Ctrl+C or docker-compose stop
```

4. After a build using "docker-compose up" the containers can be started using:

```
docker-compose start
```

If you do not use the docker and docker-compose, to run this project use the following steps.

1. Install all dependencies:

```
yarn or yarn install
```

2. To initialize the project for development, run:

```
yarn run start:dev
```

3. To initialize the project for production, run:

```
yarn run start
```

## Tests

This project uses the Jest library as test suite tool.
To run the tests use:

```
docker exec -it express-boilerplate yarn test
```

_The containers must be running!_

OR

```
yarn test
```

## Linting

This project uses the ESLint with Standard.js as lint tool.
To run linting use:

```
docker exec -it express-boilerplate yarn lint
```

_The containers must be running!_

OR

```
yarn lint
```

## Inspirations

* [danielfsousa/express-rest-es2017-boilerplate](https://github.com/danielfsousa/express-rest-es2017-boilerplate)
* [KunalKapadia/express-mongoose-es6-rest-api](https://github.com/KunalKapadia/express-mongoose-es6-rest-api)
* [ridhamtarpara/express-es8-rest-boilerplate](https://github.com/ridhamtarpara/express-es8-rest-boilerplate)
