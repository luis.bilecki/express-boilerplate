const { isTest } = require('./helpers/env')

require('dotenv').config({
  path: isTest() ? '.env.test' : '.env'
})
