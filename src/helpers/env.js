const bool = key => {
  const value = ('' + process.env[key]).trim().toLowerCase()
  if (/true|t|1|y|yes/.test(value)) {
    return true
  }

  return false
}

const isProduction = () => {
  return process.env.NODE_ENV === 'production'
}

const isDevelopment = () => {
  return process.env.NODE_ENV === 'development'
}

const isTest = () => {
  return process.env.NODE_ENV === 'test'
}

module.exports = {
  bool,
  isProduction,
  isDevelopment,
  isTest
}
