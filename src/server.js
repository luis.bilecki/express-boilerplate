require('./bootstrap')

const app = require('./app')

const loggerService = require('./services/Logger')

const PORT = parseInt(process.env.PORT) || 3000

app.listen(PORT, () => loggerService.info(`Up and running on port ${PORT}`, 'server'))
