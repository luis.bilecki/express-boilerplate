const { Router } = require('express')
const docRoutes = require('./docRoutes')
const statusRoutes = require('./statusRoutes')

const router = Router()

router.use('/status', statusRoutes)
router.use('/docs', docRoutes)

module.exports = router
