const { Router } = require('express')
const StatusController = require('../controllers/StatusController')

const router = Router()

router.get('/', StatusController.index)

module.exports = router
