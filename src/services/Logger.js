const winston = require('winston')

const dateFormat = () => new Date().toISOString()

// DateTime | LEVEL | operation | message | data
const customFormat = winston.format.printf(data => {
  const { level, message, operation, additionalData } = data
  const logElements = [dateFormat(), level.toUpperCase()]

  if (operation) {
    logElements.push(operation)
  }

  logElements.push(message)

  if (additionalData) {
    const additionalDataAsString = `data:${JSON.stringify(additionalData)}`
    logElements.push(additionalDataAsString)
  }

  return logElements.join(' | ')
})

class LoggerService {
  constructor () {
    this.logger = winston.createLogger({
      format: customFormat,
      transports: [new winston.transports.Console()]
    })
  }

  info (message, operation = null, additionalData = null) {
    this.logger.log('info', message, {
      operation,
      additionalData
    })
  }

  debug (message, operation = null, additionalData = null) {
    this.logger.log('debug', message, {
      operation,
      additionalData
    })
  }

  error (message, operation = null, additionalData = null) {
    this.logger.log('error', message, {
      operation,
      additionalData
    })
  }
}

module.exports = new LoggerService()
