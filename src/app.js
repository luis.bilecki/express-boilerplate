require('./bootstrap')

const express = require('express')
const morgan = require('morgan')
const compression = require('compression')
const helmet = require('helmet')
const cors = require('cors')
const app = express()

const loggerService = require('./services/Logger')

// Request logging
app.use(
  morgan('tiny', {
    stream: {
      write: message => {
        const parsedMessage = message.replace(/\n$/, '')
        loggerService.info(parsedMessage, 'http')
      }
    }
  })
)

// Parse body params and attach them to req.body
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

// Compression using gzip
app.use(compression())

// Secure your app by enabling various HTTP headers
app.use(helmet())

// Enable Cross Origin Resource Sharing (CORS)
// See https://www.npmjs.com/package/cors to configure cors origin and options
app.use(cors())

// Mount API routes
app.use('/api', require('./routes'))

module.exports = app
